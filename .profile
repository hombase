#case-insensitive when expand file name
shopt -s nocaseglob
set completion-ignore-case on 

# history setting
#Make Bash append rather than overwrite the history on disk
shopt -s histappend
#Whenever displaying the prompt, write the previous line to disk
PROMPT_COMMAND='history -a'
export HISTCONTROL=ignoredups
export HISTIGNORE="[   ]*:&:bg:fg:hs *:history:exit"
export HISTTIMEFORMAT="%d/%m/%y %T "
HISTFILESIZE=500000000
HISTSIZE=500000


#fortune

# for cvs env
export CVSROOT=":pserver:lifan@localhost:/l/cvsdev"
#export CVSROOT=":ext:lifan@localhost:/l/cvsdev"
#export CVS_RSH="ssh"

#for the nt proȨ��
#export CYGWIN="nontsec nosmbntsec tty"
#export CYGWIN="nodosfilewarning"

#for man conf
export MANPATH=/usr/local/man:/usr/local/share/man:/usr/share/man:/usr/man:/usr/ssl/man

#for python lib path
export PYTHONSTARTUP=/home/lifan/.pythonstartup
#set PYTHONDOCS=d:\Python\Doc
export PYTHONPATH="/usr/local/lib/python2.5/site-packages:/home/lifan/other-sh"

#for the x win path
#GDK_USE_XFT=0

#for usr path
export PATH=~/.local/bin:$PATH

#for pkg-config path
#export PKG_CONFIG_PATH="/usr/lib/pkgconfig/:/usr/local/lib/pkgconfig/"

# for the display Chinese Char
source ~/.myprofile

#from bashrc
#LC_CTYPE=zh_CN.GB2312
#CHARSET=GB2312 
LC_CTYPE=zh_CN.UTF-8
#LC_CTYPE=C
CHARSET=UTF-8
export LC_CTYPE CHARSET
#export XMODIFIERS="@im=fcitx" 
XML_CATALOG_FILES=$HOME/conf/catalog.xml\ /etc/xml/catalog
export XML_CATALOG_FILES

#clear
