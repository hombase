@ECHO OFF

if DEFINED MYGENV if NOT DEFINED MYGENV_DONE call %E_DISK%\home\%USERNAME%\other-sh\mygenv.bat

call mygexec.bat "gvim.exe --remote-tab-silent" W %*

exit 0
