@ECHO OFF

if DEFINED MYGENV if NOT DEFINED MYGENV_DONE call %E_DISK%\home\%USERNAME%\other-sh\mygenv.bat

set gtk_version=2.14.7
IF '"%~1"' == '"--gtk_version"' (
        set gtk_version=%~2
        SHIFT
        SHIFT
        )
set PATH=%PATH%;%d_disk%\prog\blush\GTK\%gtk_version%\bin;%d_disk%\prog\blush\GTK\%gtk_version%;%d_disk%\prog\blush\Leafpad

call mygexec.bat leafpad.exe W %*

exit 0
