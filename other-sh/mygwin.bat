@echo off

if DEFINED MYGENV if NOT DEFINED MYGENV_DONE call %E_DISK%\home\%USERNAME%\other-sh\mygenv.bat

if DEFINED USE_MINTTY (
    set mintty=true
    )

if '%1'=='' GOTO EMPTY

REM d:
REM chdir %CYGWIN_HOME%\bin

for /f %%i in ('cygpath -u %1') do set xpath=%%i

REM sh.exe -c "echo cd %xpath% #mint >> /e/home/lifan/.profile"
echo cd %xpath% #mint >> %E_DISK%\home\%USERNAME%\.profile
REM sh.exe -c "sed -e 's/\x0D$//g' /e/home/lifan/.profile -i"

REM bash --login -i
if '%mintty%'=='true' (
    start mintty.exe --class "PuTTY-mint"  -
)else (
    start putty.exe -load "cygwin2"
    )

sleep 5s
sh.exe -c "sed -e '/mint/d' $HOME/.profile -i"
exit 0

:EMPTY
if '%mintty%'=='true' (
    start mintty.exe --class "PuTTY-mint" -
)else (
    start putty.exe -load "cygwin2"
    )

exit 0
