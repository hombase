#!bash

if [[ $# = 1 ]]
then
	l=$1
else
	l="l"
fi
#copy firefox profile file
cd /$l/for_together/e_disk
mv autofox.tgz autofox2.tgz
cd /e
tar -cz --exclude-from /e/fe -f autofox.tgz vgoah_fox
cp -au /e/autofox.tgz /$l/for_together/e_disk 

#copy std fold's study file 
#cp -rau ~/std* /$l/for_together/d_disk/cygwin/home/administrator

#copy viki files
cp -rau ~/viki /$l/for_together/d_disk/cygwin/home/administrator
cp -rau ~/olm /$l/for_together/d_disk/cygwin/home/administrator
#cp -rau /d/prog/Palm/lee/TextSync/texts /$l/for_together/d_disk/prog/Palm/lee/TextSync

#copy flashget download file
#cd /e/temp_temp/flashget
#ls -l | awk -v wkmon="`date`" 'BEGIN { split(wkmon,wkdate)} $6 == wkdate[2] && $7 == wkdate[3] { print $9}' >tempflashget$$
#cp -auvr `cat tempflashget*` /$l/temp_temp/flashget
#rm tempflashget*

#copy firefox html save file
#cd /e/temp_temp/firefox/html
#ls -l | awk -v wkmon="`date`" 'BEGIN { split(wkmon,wkdate)} $6 == wkdate[2] && $7 == wkdate[3] { print $9}' >tempflashget$$
#cp -auvr `cat tempflashget` /$l/temp_temp/firefox/html
#rm tempflashget*
