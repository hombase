@ECHO OFF

if NOT DEFINED MYGENV goto :EOF
if DEFINED MYGENV_DON goto :EOF

REM echo here in mygenv.bat > mygenv.log

REM just for a tip how to do this
REM for /f %%i in ('%CYGWIN_HOME%\bin\cygpath -d /d') do set D_DISK=%%i


set CYGWIN_HOME=%d_disk%\cygwin
set HOME=%E_DISK%\home\%USERNAME%

if NOT DEFINED FOXVER set FOXVER=14
if NOT DEFINED BIRDVER set BIRDVER=10

set PATH=%HOME%\other-sh;%E_DISK%\local\bin;%CYGWIN_HOME%\bin;%CYGWIN_HOME%\sbin;%CYGWIN_HOME%\usr\sbin;%PATH%
set PATH=%D_DISK%\python;%D_DISK%\prog\Vim\vim73;%D_DISK%\prog\fox%FOXVER%;%D_DISK%\prog\bird%BIRDVER%;%D_DISK%\prog\Totalcmd7;%PATH%

set HTML_TIDY=%HOME%\tidy.conf

set MYGENV_DONE=true
