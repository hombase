#!bash

#softbackup2.sh--                                _describe_
#@Author:                                               LiFan
#@Created:                                              2010-02-25-00:03

PATH=/bin:/usr/bin:$PATH

error()
{
    echo "$@" 1>&2
    usage_and_exit 1
}

# next to add all array and to generate the usage message 
# can move this variable down to usage?
# all_name=???

usage()
{
    echo "Usage: $PROGRAM [--all] [--help] [--version] [--kind tar|cpio] [--write_date|d <yes|no>]"
    echo -e "\t[--write_host|-n] [--passwd] [--backup_path <the path>] [--backup_command <cp|scp|scp -P 443]"
    echo -e "\t[--utensil <list|all|item>] [autobat|litestep|fox|vgoah_fox|greenfox|homey|l_disk|local|maily|bird] ..."
    echo -e "for example"
    echo -e "backup Totalcmd7 of utensil to path /l/local-backup:"
    echo -e "\t softbackup2.sh -b /l/local-backup -c cp  -u Totalcmd7 utensil"
    echo -e "backup some other utensil:"
    echo -e "\t softbackup2.sh -b /l/local-backup -c cp -u FindAndRunRobot -u Everything-1.2.1.371 -u CintaNotes1.2 -u ditto-3.16.8.0  utensil"
    echo -e "backup home with password:"
    echo -e "\t softbackup2.sh -b /l/local-backup -c cp -p homey"
    echo -e "list the module of utensil"
    echo -e "\t softbackup2.sh -u list utensil"
}

usage_and_exit()
{
    usage
    exit $1
}

version()
{
    echo "$PROGRAM version $VERSION"
}

warning()
{
    echo "$@" 1>&2
    EXITCODE=$((EXITCODE + 1))
    #EXITCODE=`expr $EXITCODE + 1`
}

EXITCODE=0
PROGRAM=`basename $0`
VERSION=1.0
all=no
kind=cpio
passwd=
utensil_command=
write_date=yes
write_host=no
fox_version=14
bird_version=10

HOSTIP=192.168.1.98
BACKUPPATH=$HOSTIP:/cygdrive/i/back_soft
BACKUPCOMMAND="scp -P 443 "

while test $# -gt 0
do
    case $1 in
        --all | -a)
            all=yes
            ;;
        --help | -h)
            usage_and_exit 0
            ;;
        --version | -v)
            version
            exit 0
            ;;
        --kind|-k)
            shift
            kind=$1
            ;;
        --passwd|-p)
            stty -echo    # Turns off screen echo.
            echo -n "Enter password:"
            read passwd
            stty echo     # Restores screen echo.
            ;;
        --backup_path|-b)
            shift
            BACKUPPATH=$1
            ;;
        --backup_command|-c)
            shift
            BACKUPCOMMAND=$1
            ;;
        --utensil|-u)
            shift
            utensil_command="$utensil_command $1"
            ;;
        --write_date|-d)
            shift
            write_date=$i
            ;;
        --write_host|-n)
            write_host=yes
            ;;
        -*)
            error "Unrecognized option: $1"
            ;;
        *)
            break
            ;;
    esac
    shift
done

#----------- end of phrase argument ---------------#

if [ $# -eq 0 -a $all != "yes" ]
then
    usage_and_exit 0
fi

if [ $all = "yes" ]
then
    backup="autobat litestep fox vgoah_fox greenfox homey l_disk local maily utensil"
else
    backup=$*
fi

if [ "$write_date" = yes ]
then
    d=`date +%Y-%b-%d-%H%M%S`
fi

if [ "$write_host" = yes ]
then
    n=`hostname`
fi

for i in $backup
do
    case $i in
        autobat)
            echo "backup autoexec.bat ..."
            $BACKUPCOMMAND /c/autoexec.bat $BACKUPPATH/c_disk
            ;;
        litestep)
            cd /d
            if [ "$kind" = cpio ]
            then
                find LiteStep -print | grep -v .svn | cpio -oav | lzma -z > litestep$d$n.cpio.lzma
                $BACKUPCOMMAND litestep$d$n.cpio.lzma $BACKUPPATH/d_disk
                rm litestep$d$n.cpio.lzma
            else
                tar cjvf LiteStep.tar.bz2 LiteStep
                $BACKUPCOMMAND LiteStep.tar.bz2 $BACKUPPATH/d_disk
                rm LiteStep.tar.bz2
            fi
            ;;
        fox)
            cd /d/prog
            if [ "$kind" = cpio ]
            then
                d=`date +%Y-%b-%d-%H%M%S`
                find fox$fox_version -print | grep -v .svn | cpio -oav | lzma -z > "fox$fox_version$d$n.cpio.lzma"
                $BACKUPCOMMAND fox$fox_version*.cpio.lzma $BACKUPPATH/d_prog/
                rm -f fox$fox_version*.cpio.lzma
            else
                tar cjvf fox$fox_version.tar.bz2 fox$fox_version
                $BACKUPCOMMAND *.bz2 $BACKUPPATH/d_prog/
                rm -f *.bz2
            fi
            ;;
        bird)

            cd /d/prog
            if [ "$kind" = cpio ]
            then
                d=`date +%Y-%b-%d-%H%M%S`
                find bird$bird_version -print | grep -v .svn | cpio -oav | lzma -z > "bird$bird_version$d$n.cpio.lzma"
                $BACKUPCOMMAND bird$bird_version*.cpio.lzma $BACKUPPATH/d_prog/
                rm -f bird$bird_version*.cpio.lzma
            else
                :
            fi
            ;;
        vgoah_bird)
            cd /e
            if [ "$kind" = cpio ]
            then
                d=`date +%Y-%b-%d-%H%M%S`
                if [ -z $passwd ]
                then
                    find vgoah_bird$bird_version/ -print | grep -f /e/vgoah_bird$bird_version/readme/fe_cpio -v | grep -v .svn | cpio -oav | lzma -z > "vgoah_bird$bird_version$d$n.cpio.lzma"
                else
                    find vgoah_bird$bird_version/ -print | grep -f /e/vgoah_bird$bird_version/readme/fe_cpio -v | grep -v .svn | cpio -oav | lzma -z | ccrypt -ef -K "$passwd" > "vgoah_bird$bird_version$d$n.cpio.lzma"
                fi
                $BACKUPCOMMAND vgoah_bird$bird_version*.cpio.lzma $BACKUPPATH/e_disk
                rm -f vgoah_bird$bird_version*.cpio.lzma
            else
                :
            fi
            ;;
        vgoah_fox)
            cd /e
            if [ "$kind" = cpio ]
            then
                d=`date +%Y-%b-%d-%H%M%S`
                if [ -z $passwd ]
                then
                    find vgoah_fox$fox_version/ -print | grep -f /e/vgoah_fox$fox_version/readme/fe_cpio -v | grep -v .svn | cpio -oav | lzma -z > "vgoah_fox$fox_version$d$n.cpio.lzma"
                else
                    find vgoah_fox$fox_version/ -print | grep -f /e/vgoah_fox$fox_version/readme/fe_cpio -v | grep -v .svn | cpio -oav | lzma -z | ccrypt -ef -K "$passwd" > "vgoah_fox$fox_version$d$n.cpio.lzma"
                fi
                $BACKUPCOMMAND vgoah_fox$fox_version*.cpio.lzma $BACKUPPATH/e_disk
                rm -f vgoah_fox$fox_version*.cpio.lzma
            else
                tar -cjv --exclude-from /e/vgoah_fox$fox_version/readme/fe -f vgoah_fox$fox_version.tar.bz2 vgoah_fox$fox_version
                $BACKUPCOMMAND vgoah_fox$fox_version*.bz2 $BACKUPPATH/e_disk
                rm -f vgoah_fox$fox_version*.bz2
            fi
            ;;
        greenfox)
            ##cd /e/vgoah_fox$fox_version
            ##gawk 'BEGIN {flag = 0;}/once start/ {flag = 1;} {if(flag == 0){print $0;}else{print substr($0,3)}} ' user.js > user2.js
            ##cp -f user.js user.js.notonce
            ##mv -f user2.js user.js
            cd /e
            if [ "$kind" = cpio ]
            then
                find vgoah_fox$fox_version/ -print | grep -f /e/vgoah_fox$fox_version/readme/fe_green_cpio -v | grep -v .svn | cpio -oav | lzma -z > "green_vgoah_fox$fox_version$d$n.cpio.lzma"
                $BACKUPCOMMAND green*.cpio.lzma $BACKUPPATH/e_disk
                rm -f green*.cpio.lzma
            else
                tar -cjv --exclude-from /e/vgoah_fox$fox_version/readme/fe_green  -f green_vgoah_fox$fox_version.tar.bz2 vgoah_fox$fox_version
                $BACKUPCOMMAND green*.bz2 $BACKUPPATH/e_disk
                rm -f green*.bz2
            fi
            ##cd vgoah_fox$fox_version
            ##mv -f user.js.notonce user.js
            ;;

        thinbird)
            cd /e
            if [ "$kind" = cpio ]
            then
                if [ -z $passwd ]
                then
                    find vgoah_bird$bird_version/ -print | grep -f /e/vgoah_bird$bird_version/readme/fe_thin_cpio -v | grep -v .svn | cpio -oav | lzma -z > "thin_bird$bird_version$d$n.cpio.lzma"
                else
                    find vgoah_bird$bird_version/ -print | grep -f /e/vgoah_bird$bird_version/readme/fe_thin_cpio -v | grep -v .svn | cpio -oav | lzma -z | ccrypt -ef -K "$passwd" > "thin_bird$bird_version$d$n.cpio.lzma"
                fi
                $BACKUPCOMMAND thin*cpio.lzma $BACKUPPATH/e_disk
                rm -f thin*.cpio.lzma
            else
                :
            fi
            ;;
        greenbird)
            cd /e
            if [ "$kind" = cpio ]
            then
                find vgoah_bird$bird_version/ -print | grep -f /e/vgoah_bird$bird_version/readme/fe_green_cpio -v | grep -v .svn | cpio -oav | lzma -z > "green_bird$bird_version$d$n.cpio.lzma"
                $BACKUPCOMMAND green*.cpio.lzma $BACKUPPATH/e_disk
                rm -f green*.cpio.lzma
            else
                :
            fi
            ;;
        homey)
            cd /home/lifan
            if [ kind='cpio' ]
            then
                if [ -z $passwd ]
                then
                    find `cat home.content` -print | grep -v .mutt/cache | grep -v .svn| grep -v rainlendar2/backups | cpio -oav | lzma -z > "../home$d$n.cpio.lzma"
                else
                    find `cat home.content` -print | grep -v .mutt/cache | grep -v .svn| grep -v rainlendar2/backups | cpio -oav | lzma -z | ccrypt -K $passwd > "../home$d$n.cpio.lzma"
                fi

                $BACKUPCOMMAND ../home*.cpio.lzma $BACKUPPATH/homey
                rm -f ../home*.cpio.lzma
            else
                tar cjvf ../home.tar.bz2 --files-from home.content
                $BACKUPCOMMAND ../home.tar.bz2 $BACKUPPATH/homey
                rm -f ../home*.bz2
            fi
            ;;
        l_disk)
            cd /l
            if [ kind='cpio' ]
            then
                find `cat l_disk.content` -print | cpio -oav | lzma -z > l_disk$d$n.cpio.lzma
                $BACKUPCOMMAND l_disk$d$n.cpio.lzma $BACKUPPATH/homey
                rm -f l_disk$d$n.cpio.lzma
            else
                tar cjvf l_disk.tar.bz2 --files-from l_disk.content
                $BACKUPCOMMAND l_disk.tar.bz2 $BACKUPPATH/homey
                rm -f l_disk.tar.bz2
            fi
            ;;
        local)
            cd /e
            if [ "$kind" = cpio ]
            then
                if [ -z $passwd ]
                then
                    find local -print | grep -v .svn | cpio -oav | lzma -z > "local$d$n.cpio.lzma"
                else
                    find local -print | grep -v .svn | cpio -oav | lzma -z | ccrypt -K $passwd > "local$d$n.cpio.lzma"
                fi

                $BACKUPCOMMAND local*.cpio.lzma $BACKUPPATH/homey
                rm -f local*.cpio.lzma
            else
                tar cjvf local.tar.bz2 local
                $BACKUPCOMMAND local.tar.bz $BACKUPPATH/homey
                rm -f local.tar.bz2
            fi
            ;;
        maily)
            cd /home/lifan
            if [ "$kind" = cpio ]
            then
                if [ -z $passwd ]
                then
                    find Mail -print | grep -v .svn | cpio -oav | lzma -z > "maily$d$n.cpio.lzma"
                else
                    find Mail -print | grep -v .svn | cpio -oav | lzma -z | ccrypt -K $passwd > "maily$d$n.cpio.lzma"
                fi
                $BACKUPCOMMAND "maily$d$n.cpio.lzma" $BACKUPPATH/maily
                rm -f *.lzma
            else
                echo "no tar methond"
            fi
            ;;
        utensil)
            echo $utensil_command
            case $utensil_command in
                *all*)    
                    utensil_backup="FindAndRunRobot Everything-1.2.1.371 CintaNotes1.2 ditto-3.16.8.0 AutoHotkey Totalcmd7 Vim freeime mpui Foobar2000 IrfanView"
                    ;;
                *list*)
                    echo -e "\tFindAndRunRobot\n\tEverything-1.2.1.371\n\tCintaNotes1.2\n\tditto-3.16.8.0\n\tAutoHotkey\n\tTotalcmd7\n\tVim\n\tfreeime\n\tmpui\n\tFoobar2000\n\tIrfanView\n"
                    exit 0
                    ;;
                *)
                    utensil_backup=$utensil_command
            esac

            for i in $utensil_backup
            do
                if [ -d /d/candel/$i ]
                then
                    cd /d/candel
                elif [ -d /d/prog/$i ]
                then
                    cd /d/prog
                else
                    echo no path: $i
                    exit 267
                fi
                find $i -print |grep -v '\(Ditto\|Everything\|Thumbs\).db$'|grep -v '.svn' | cpio -oav | lzma -z > $i$d$n.cpio.lzma
                $BACKUPCOMMAND *.cpio.lzma $BACKUPPATH/utensil
                rm -f *.lzma
            done
            ;;
    esac
done
