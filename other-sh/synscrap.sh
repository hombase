#!/usr/bin/env bash

#synscrap.sh--                                _describe_
#@Author:                                               LiFan
#@Created:                                              2010-02-23-23:09

PATH=/bin:/usr/bin:$PATH
rsync -avzub /l/scrapbook/ 192.168.1.98:/l/scrapbook/
rsync -avzub 192.168.1.98:/l/scrapbook/ /l/scrapbook/
