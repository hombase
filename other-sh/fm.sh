#!/bin/sh

#fm.sh--                                _describe_
#@Author:                                               LiFan
#@Created:                                              2010-09-24-21:10

PATH=/bin:/usr/bin:/usr/local/bin

error()
{
    echo "$@" 1>&2
    usage_and_exit 1
}

usage()
{
    echo "Usage: $PROGRAM [--all] [--help] [--version] ..."
}

usage_and_exit()
{
    usage
    exit $1
}

version()
{
    echo "$PROGRAM version $VERSION"
}

warning()
{
    echo "$@" 1>&2
    EXITCODE=$((EXITCODE + 1))
    #EXITCODE=`expr $EXITCODE + 1`
}


EXITCODE=0
PROGRAM=`basename $0`
VERSION=1.0
all=no

while test $# -gt 0
do
    case $1 in
        --all | -a)
        all=yes
        ;;
        --help | -h)
        usage_and_exit 0
        ;;
        --version | -v)
        version
        exit 0
        ;;
        -*)
        error "Unrecognized option: $1"
        ;;
        *)
        break
        ;;
    esac
    shift
done

#----------- end of phrase argument ---------------#
fetchmail -L "" --ssl -p IMAP -kc --sslcertck --sslcertpath /home/lifan/.mutt/certs  -u vgoah10@gmail.com imap.gmail.com|gawk '{printf $2  $4 ") gmail\n"}'

#fetchmail -L "" -p IMAP -kc  -u vgoah10 imap.yeah.net 2>&1|gawk '{printf $2  $4 ") yeah\n"}' |tail -n1
fetchmail -L "" -p IMAP -kc  -u af2002 imap.126.com 2>&1|gawk '{printf $2  $4 ") af2002\n"}' |tail -n1

fetchmail -L "" -k|head -n 1|gawk '{printf $2  $4 ") china\n"}'
