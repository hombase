@ECHO OFF

set exec=%~1
shift

rem k is king of template: CYG CYG_X(xwin), CYG_M(mail relevant) or W(window)
set kind=%1
shift

IF '%1' == '' (
        if '%kind%'=='CYG_X' (
            sh.exe -login -c "export DISPLAY=:1;%exec%"
            exit 0
            )else (
                start %exec%
                exit 0
                )
        )

IF '"%~1"' == '"--argulist"' (
        IF %kind% == W shift & goto FILE
        IF %kind% == CYG shift & goto CYG_FILE
        IF %kind% == CYG_X shift & set cyg_x=true & goto CYG_FILE
        IF %kind% == CYG_M shift & set cyg_m=-a & goto CYG_FILE
        exit 0
        )

IF %kind% == W goto COMM
IF %kind% == CYG goto CYG
IF %kind% == CYG_X set cyg_x=true & goto CYG
IF %kind% == CYG_M set cyg_m=-a & goto CYG

exit 0

REM -------------------------------------------------------------------------
:COMM
:COMM_LOOP
IF '%1' =='' GOTO COMM_ENDLOOP
set vars=%vars% %1
SHIFT
GOTO COMM_LOOP
:COMM_ENDLOOP
start %exec% %vars%
goto :EOF

REM -------------------------------------------------------------------------
:FILE
REM must using %~1 to remove " , if not will not read content in %1 
for /f "delims=" %%i in (%~1) do (
    call set dosvars=%%dosvars%% %%i
    )

start %exec% %dosvars%
goto :EOF

REM -------------------------------------------------------------------------
:CYG
:CYG_LOOP
IF '%1' =='' GOTO CYG_ENDLOOP
for /f %%i in ('cygpath -d %1') do set shortpath=%%i
for /f %%i in ('cygpath -u %shortpath%') do set xpath=%%i
set vars=%vars% %cyg_m% %xpath%
SHIFT
GOTO CYG_LOOP
:CYG_ENDLOOP

if DEFINED cyg_x (
    sh.exe -login -c "export DISPLAY=:1;%exec% %vars%"
)else (
    start %exec% %vars%
)
goto :EOF

REM -------------------------------------------------------------------------
:CYG_FILE
REM must using %~1 to remove " , if not will not read content in %1
for /f "delims=" %%i in (%~1) do (
    call set dosvars=%%dosvars%% %%i
    )

for /f %%i in ('cygpath -d %dosvars%') do (
    call set shortpaths=%%shortpaths%% %%i
    )

for /f %%i in ('cygpath -u %shortpaths%') do (
    call set vars=%%vars%% %cyg_m% %%i
    )

if DEFINED cyg_x (
        sh.exe -login -c "export DISPLAY=:1;%exec% %vars%"
        )else (
            start %exec% %vars%
            )

