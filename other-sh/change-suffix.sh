#!bash

#change-suffix.sh--		change all the same suffix a to b
#@Author:				LiFan
#@Created:				2006-Jul-24-11-19

PATH=/bin:/usr/bin:$PATH

a=$1
b=$2
for i in `ls *."$a"`
do
	mv $i `echo $i | awk -F. '{printf "%s.",$1}'`$b
done
