#!bash
if [[ $# != 1 ]]
then
	echo "please input the backup path"
	exit 2
fi

echo $1
cd $1
mv cvsroot.tgz cvsroot2.tgz -f
mv cvsntroot.tgz cvsntroot2.tgz -f
cd /l
tar czvf $1/cvsroot.tgz cvsroot
tar czvf $1/cvsntroot.tgz cvsntroot
