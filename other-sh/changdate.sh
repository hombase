#!bash

#_netide.sh--				_describe_
#@Author:				LiFan
#@Created:				2007-01-22-23:25

PATH=/bin:/usr/bin:$PATH
olddate=`date +%D`
date -s "$1 `date +%T`"
run "$3" -wait
sleep $2
date -s "$olddate `date +%T`"

