#!/bin/sh

#rsync-all.sh--                                _describe_
#@Author:                                               LiFan
#@Created:                                              2011-04-02-11:49

PATH=/usr/local/bin:/bin:/usr/bin

error()
{
    echo "$@" 1>&2
    usage_and_exit 1
}

usage()
{
    echo "Usage: $PROGRAM [--all] [--list] [--athome|-m] [--mode|-o <put|get|put-update|get-update>] [--dry|-n] <--tohost|-t> [--localroot|-r][--help] [--version] [the component...]"
    echo "rsync to ip98:"
    echo -e "\trsync-all.sh -a -t ip98:"
    echo -e "\trsync-all.sh -t ip98: update"
    echo -e "\trsync-all.sh -t ip98: fox"
    echo "rsync to 1T:"
    echo -e "\trsync-all.sh -t ip98:/cygdrive/h/li\@58 update"
    echo -e "\trsync-all.sh -t ip98:/cygdrive/h/li\@58 local-backup"
    #echo "rsync to 67.195 at localhost: by port 32195"
    #echo -e "\trsync-all.sh localhost: qa 32195"
    echo "rsync to ip 108"
    echo -e "\trsync-all.sh -t 192.168.1.108: update"
    echo "rsync to local path /cygdrive/h"
    echo -e "\trsync-all.sh -t /cygdrive/h update"
    echo "rsync to tu58 for get update and just test"
    echo -e "\trsync-all.sh -o get -n -t tu58: update"
    echo "rsync to 192.168.67.242 for get update and just test on mac"
    echo -e "\trsync-all.sh -o get -n -t 192.168.67.242: -r /Volumes/hard update"
}

usage_and_exit()
{
    usage
    exit $1
}

version()
{
    echo "$PROGRAM version $VERSION"
}

warning()
{
    echo "$@" 1>&2
    EXITCODE=$((EXITCODE + 1))
    #EXITCODE=`expr $EXITCODE + 1`
}


EXITCODE=0
PROGRAM=`basename $0`
VERSION=1.0
all=no
mode=put
dry=false
user=lifan
port=22
#components=
hosts=
localroot=""

hostname=`hostname`
if [ $hostname = "football12" ]
then
    athome=yes
else
    athome=no
fi


while test $# -gt 0
do
    case $1 in
        --all | -a)
            all=yes
            ;;
        --help | -h)
            usage_and_exit 0
            ;;
        --version | -v)
            version
            exit 0
            ;;
        --list | -l)
            all=list
            ;;
        --athome| -m)
            athome=yes
            ;;
        --mode| -o)
            shift
            mode=$1
            ;;
        --dry-run| -n)
            dry=true
            ;;
        --tohost| -t)
            shift
            hosts="$hosts $1"
            ;;
        --localroot| -r)
            shift
            localroot="$1"
            ;;
        -*)
            error "Unrecognized option: $1"
            ;;
        *)
            break
            ;;
    esac
    shift
done

#----------- end of phrase argument ---------------#

rsync1()
{
    echo "in rsync1"
    host1=$1
    shift
    host2=$1
    shift
    echo rsync $* $host1 $host2
    rsync $* $host1 $host2
}

rsync2()
{
    echo "in rsync2"
    host1=$1
    shift
    host2=$1
    shift
    echo rsync $* $host2 $host1
    rsync $* $host2 $host1
}

if [ "$all" = "list" ]
then
    echo -e "componets:\n\tupdate: /e/update /e/ori\n\tfox: /l/scrapbook/\n\tmiranda\n\tlb: /l/local-backup/\n\t"
    exit 0
fi


if [ -z "$hosts" ]
then
    error "tohost must set!!!"
fi


if [ $all == "yes" ]
then
    echo "use all setting, means sync update and fox"
    componets="update fox"
else
    if [ $# -eq 0 ]
    then
        error "component or -a must set!!!"
    else
        echo $*
        components=$*
    fi
fi

# -a: archive mode, equivalent to -rlptgoD
# -v: increase verbosity
# -z: compress file data
# --delete: This tells rsync to delete any files on the receiving side that aren't on the send-ing  side
case $mode in
    put)
        echo "use mode put and delete"
        ARGU="-avz --delete"
        rsync_func=rsync1
        ;;
    get)
        echo "use mode get and delete"
        ARGU="-avz --delete"
        rsync_func=rsync2
        ;;
    put-update)
        echo "use mode put and update and not delete"
        ARGU="-avzub"
        rsync_func=rsync1
        ;;
    get-update)
        echo "use mode get and update and not delete"
        ARGU="-avzub"
        rsync_func=rsync2
        ;;
    *)
        error "mode error"
        exit 1
        ;;
esac

if [ $dry == "true" ]
then
    ARGU="$ARGU --dry-run"
fi

echo $components
for i in $components
do
    case $i in
        update)
            echo "in update component..."
            # can use i again at here ???
            for j in $hosts
            do
                echo rsync to $j...
                $rsync_func $localroot/e/ori/ $j/e/ori/ $ARGU
                $rsync_func $localroot/e/update/ $j/e/update/ $ARGU
            done
            ;;
        fox)
            echo "in fox component ..."
            for j in $hosts
            do
                echo rsync to $j...
                $rsync_func $localroot/l/scrapbook/ $j/l_disk/scrapbook/ $ARGU
                $rsync_func $HOME/syncplaces.json $j/e/home/$user/syncplaces.json $ARGU
                $rsync_func $HOME/syncplaces.json.sha1 $j/e/home/$user/syncplaces.json.sha1 $ARGU
                $rsync_func $localroot/d/temp.txt $j/d/temp.txt $ARGU
            done
            ;;
        miranda)
            echo "in miranda componet ..."
            for j in $hosts
            do
                echo rsync to $j...
                $rsync_func $localroot/d/prog/miranda-ing/ $j/d/prog/miranda-ing/ $ARGU
            done
            ;;
        lb)
            echo "in local-backup componet ..."
            for j in $hosts
            do
                echo rsync to $j...
                $rsync_func $localroot/l/local-backup/ $j/e/l_disk/local-backup/ $ARGU
            done
            ;;
        *)
            echo "in $i component ..."
            for j in $hosts
            do
                echo rsync to $j...
                $rsync_func $localroot/$i/ $j/$i/ $ARGU
            done
            ;;
    esac
done
