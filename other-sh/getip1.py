#!/usr/bin/python

import urllib2
import re

def getip():
    theurl = 'http://192.168.67.241:8088/userRpm/StatusRpm.htm?'
    username = 'admin'
    password = 'admin'
# a great password

    passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
# this creates a password manager
    passman.add_password(None, theurl, username, password)
# because we have put None at the start it will always use this
# username/password combination for  urls for which `theurl` is a super-url

    authhandler = urllib2.HTTPBasicAuthHandler(passman)
# create the AuthHandler

    opener = urllib2.build_opener(authhandler)

    urllib2.install_opener(opener)
# All calls to urllib2.urlopen will now use our handler Make sure not to
# include the protocol in with the URL, or HTTPPasswordMgrWithDefaultRealm will
# be very confused.  You must (of course) use it when fetching the page though.

    try:
        pagehandle = urllib2.urlopen(theurl)
    except urllib2.URLError:
        return 'No route to host'
# authentication is now handled automatically for us

    thepage = pagehandle.read()
    pagehandle.close

    pa = re.compile(r'(var wanPara = new Array\(.*0, "[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]-[0-9A-F][0-9A-F]", ")(\d+.\d+.\d+.\d+)', re.DOTALL) 

    m = pa.search(thepage)

    return m.group(2)

if __name__=='__main__':
    print getip()
