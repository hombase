#!/bin/sh

if ps -asW | egrep -i xwin > /dev/null
then
    exit 0
fi

xwin -multiwindow -ac -dpi 85 :1&
sleep 10

# to find the display
export DISPLAY=:1
xset +fp /usr/local/share/fonts/zh_CN
xset +fp /usr/local/share/fonts/wenquanyi

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap

if [ -f "$userresources" ]
then
    xrdb -merge "$userresources"
fi

if [ -f "$usermodmap" ]
then
    xmodmap "$usermodmap"
fi
