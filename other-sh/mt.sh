#!/bin/sh

#mt.sh--                                _describe_
#@Author:                                               LiFan
#@Created:                                              2011-01-22-15:20

PATH=/bin:/usr/bin:/usr/local/bin

error()
{
    echo "$@" 1>&2
    usage_and_exit 1
}

usage()
{
    echo "Usage: $PROGRAM [--list] [--all|-a] [--athome|-m] [--proxy|-x] [--prox-home|-xh] [--help] [--version] [host-name]"
    echo -e "for example:"
    echo -e "\tmt.sh --list"
    echo -e "\tmt.sh qapc27"
    echo -e "\tmt.sh -x 192.168.79.xx tu98"
    echo -e "\tmt.sh x103-22"
    echo -e "\tmt.sh -xh 192.168.67.245 others"
}

usage_and_exit()
{
    usage
    exit $1
}

version()
{
    echo "$PROGRAM version $VERSION"
}

warning()
{
    echo "$@" 1>&2
    EXITCODE=$((EXITCODE + 1))
    #EXITCODE=`expr $EXITCODE + 1`
}


EXITCODE=0
PROGRAM=`basename $0`
VERSION=1.0
HOMEPROXY=192.168.67.245
SECONDPROXY=192.168.72.103
THIRDPROXY=
QAPROXY=qaproxy
all=no
athome=no

hostname=`hostname`
if [[ $hostname = "football12" ]]
then
    athome=yes
fi

option=conn

while test $# -gt 0
do
    case $1 in
        --all | -a)
        all=yes
        ;;
        --help | -h)
        usage_and_exit 0
        ;;
        --version | -v)
        version
        exit 0
        ;;
        --list | -l)
        option=list
        ;;
        --athome| -m)
        athome=yes
        ;;
        --proxy| -x)
        shift
        THIRDPROXY=$1
        ;;
        --proxy-home| -xh)
        shift
        HOMEPROXY=$1
        ;;
        -*)
        error "Unrecognized option: $1"
        ;;
        *)
        break
        ;;
    esac
    shift
done

#----------- end of phrase argument ---------------#

# make tunnel from local to destination by ssh server
# $1    local port
# $2    remote host name 
# $3    remote port
# $4    sshd server
# $5    record string for sed
mt()
{
    echo -e "\nconnect from local port $1 to $4 for $2:$3 ..."
    if grep -q $5 $HOME/tunnel.list
    then
        echo -e "\tyes, $5 in tunnel.list"
        if test -n "`grep $5 $HOME/tunnel.list|cut -d " " -f2`" && ps -ef|grep " `grep $5 $HOME/tunnel.list|cut -d " " -f2`" |grep -q ssh$
        then
            echo -e "\talready connected, ignore $5"
            return
        fi
    else
        echo -e "\tno, $5 not in tunnel.list, add it"
        echo $5 $pid >> $HOME/tunnel.list
    fi

    ssh -N -f -L *:$1:$2:$3 $4
    pid=`ps -ef|grep ssh$|tail -1|awk '{print $2}'`

    echo -e "\t$2:$3 $pid"
    sed -e "s/$5.*$/$5 $pid/g" $HOME/tunnel.list -i

}


# make tunnel from local to destination by ssh server
# $1:local port $2:remote host name $3:remote port
maketunnel()
{
    if [ $athome = "yes" ]
    then
        echo "nothing for $2:$3 now"
        #mt $1 $SECONDPROXY $1 $HOMEPROXY "$2:$3"
    else
        mt $1 $2 $3 $QAPROXY "$2:$3"
    fi

}

listtunnel()
{
    cat $HOME/tunnel.list | while read line
    do 
        if ps -ef|grep " `echo $line|cut -d " " -f2`" |grep -q ssh$
        then
            echo $line
        else
            :
            #echo $line |cut -d " " -f1
        fi
    done
}

if [ $option = "list" ]
then
    listtunnel
    exit 0
fi

if [ $# -eq 0 -a $all != "yes" ]
then
    usage_and_exit 0
fi


if [ $all = "yes" ]
then
    echo "use all setting, means connet to qasox02, qapc27 qapc42 qapc45 qapc59 and others ..."
    tunnel="qasox02 qapc27 qapc42 qapc45 qapc59 others"
else
    echo "connet to $* ..."
    tunnel=$*
fi

for i in $tunnel
do
    case $i in
        qasox02)
            maketunnel 10002 "qasox02.grid.datasynapse.com" 22
            ;;
        qasox06)
            maketunnel 10006 "qasox06.grid.datasynapse.com" 22
            ;;
        qa51)
            maketunnel 10051 "qa51.grid.datasynapse.com" 22
            ;;
        qapc42)
            maketunnel 9942 "qapc42.grid.datasynapse.com" 3389
            ;;
        qapc42-22)
            maketunnel 9242 "qapc42.grid.datasynapse.com" 22
            ;;
        qapc45)
            maketunnel 9945 "qapc45.grid.datasynapse.com" 3389
            ;;
        qapc27)
            maketunnel 9927 "qapc27.grid.datasynapse.com" 3389
            ;;
        qapc59-22)
            maketunnel 9259 "qapc59.grid.datasynapse.com" 22
            ;;
        qapc59)
            maketunnel 9959 "qapc59.grid.datasynapse.com" 3389
            ;;
        tu12)
            mt 20058 "192.168.67.243" 3389 $THIRDPROXY "192.168.67.243:3389"
            ;;
        tu12-22)
            mt 22058 "192.168.67.243" 22 $THIRDPROXY "192.168.67.243:22"
            ;;
        tuc2)
            mt 20098 "192.168.67.242" 3389 $THIRDPROXY "192.168.67.242:3389"
            ;;
        tuc2-22)
            mt 22098 "192.168.67.242" 443 $THIRDPROXY "192.168.67.242:443"
            ;;
        x103)
            mt 30103 "192.168.72.103" 3389 $HOMEPROXY "192.168.72.103:3389"
            ;;
        x103-22)
            mt 32103 "192.168.72.103" 22 $HOMEPROXY "192.168.72.103:22"
            ;;
        others)
            if [ $athome = "yes" ]
            then
                mt 30195 192.168.67.195 3389 $HOMEPROXY "192.168.67.195:3389"
                mt 32195 192.168.67.195 22 $HOMEPROXY "192.168.67.195:22"
            else
                #maketunnel 9965 "qapc65.grid.datasynapse.com" 3389
                #maketunnel 9968 "qapc68.grid.datasynapse.com" 3389
                #maketunnel 9970 "qapc70.grid.datasynapse.com" 3389
                #maketunnel 9973 "qapc73.grid.datasynapse.com" 3389
                #maketunnel 9978 "qapc78.grid.datasynapse.com" 3389
                maketunnel 9907 "qapcx07.grid.datasynapse.com" 3389
                maketunnel 9966 "qapc66.grid.datasynapse.com" 3389
            fi

    esac
done
