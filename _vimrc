"@author: 		    Lifan
"@Modified: 		2013-May-27-18-13

"set nocompatible 	"because this is defined in vimrc_example.vim

"source $VIMRUNTIME/vimrc_example.vim
set fileformats=unix,dos
"set encoding=utf-8
let $LANG = 'en'
"if !has("unix")
    "language message en
    ""language time English_United States.1252
    ""set langmenu=en.UTF-8
"endif

set encoding=utf-8                     " better default than latin1
setglobal fileencoding=utf-8
set fencs=ucs-bom,utf-8,gb2312,gb18030,gbk,big5,l439atin1
"set fencs=ucs-bom,utf-8,cp936,gb18030,gbk,big5,euc-jp,euc-kr,latin1
"set fencs=ucs-bom,utf-8,gb2312,gbk,gb18030,latin1
"set fencs=ucs-bom,chinese,taiwan,japan,korea,utf-8,latin1
let mapleader = ","

if has("unix")
    if has("mac")
        "let mac option key work as meta key in vim
        set macmeta
    endif
	if has("gui_running")  "for gdk+ vim seting
		"set langmenu=none
		"language en_US
		"set enc=gb2312
		"source $VIMRUNTIME/delmenu.vim
		"source $VIMRUNTIME/menu.vim
		"set imactivatekey=C-space
		"set iminsert=0
		"set ims=0
		"inoremap <esc> <esc>:set iminsert=2<cr>
		"noremap i	:set iminsert=2<cr>i
		"noremap a :set iminsert=2<cr>a
		"set ims=0
		"set imc!
		
		" CTRL-A is Select all
		noremap <C-A> gggH<C-O>G
		inoremap <C-A> <C-O>gg<C-O>gH<C-O>G
		cnoremap <C-A> <C-C>gggH<C-O>G
		onoremap <C-A> <C-C>gggH<C-O>G
		snoremap <C-A> <C-C>gggH<C-O>G
		xnoremap <C-A> <C-C>ggVG

		" CTRL-V and SHIFT-Insert are Paste
		imap <C-V> <C-r>* 
		map <C-V>		"*gP
		map <S-Insert>		"*gP
		cmap <C-V>		<C-R>*
		cmap <S-Insert>		<C-R>*
	else 		"for bash vim setting
		set noshowcmd
		colorscheme desert
		"syntax off
	endif
	"let $VIMFILY='~/.vim'
	let $VIMFILY=expand("$HOME") . '/.vim'
	"set rtp^=~/vimfiles
	"source ~/vimfiles/filetype.vim
elseif has("win32") || has("win64")
	if has("gui_running")
		"source $VIMRUNTIME/mswin.vim
		"set guioptions+=a 	"auto copy to 
		"understand this
		set grepprg=grep\ -n\ $*\ /dev/null
		set grepformat=%f:%l:%m,%f:%l%m,%f\	%l%m
		cabbrev sort /bin/sort
	else
		set noshowcmd
	endif
	let $VIMFILY=expand("$HOME") . '/vimfiles'
endif

let $ADDED=$VIMFILY . '/added_plugin'

source $ADDED/my_vimrc_example.vim
if (has("win32") || has("win64")) && has("gui_running")
    source $ADDED/my_mswin.vim
endif

source $ADDED/publib.vim

"for latex suit now
let $ADDED2=$VIM . '/vimfiles/added_plugin2'



"add this by lifan
""""""""""""""""""""""""""""""""""""""""""""""""""""""
"General
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set helplang=cn
set hh=999 	"set the help windows defaul height to max
set linebreak 	"let english word break whole word
set showmatch
set mat=3
set history=500000
set viminfo='100,<1500,s500,:500,/300
set nohlsearch
set incsearch
set timeoutlen=1500
set hid 	"let you can change buffer without save
set ignorecase
"turn off ignorecase in a typed search if an uppercase char exists.
set smartcase
call CodeMode()		"most time are coding:)
set noerrorbells
set visualbell t_vb=
set cmdheight=1
set guioptions-=T 	"hide the toolbar
set guioptions-=m 	"hide the menubar
"set guioptions+=c 	"use text dialog
set laststatus=2
set noruler
set shortmess=at
"set rulerformat=%50(\ %n[%{&ff}]%r%m%y%=%-14.(%l,%c%V%)\ %p%%%)
"set statusline=%<%f %n[%{&ff}]%r%m%y%=%-14.(%l,%c%V%)\ %p%%
hi User1 term=standout,bold cterm=reverse ctermbg=red gui=bold guifg=red guibg=Grey
hi User2 term=standout,bold cterm=reverse gui=bold guibg=Grey


"""""""""""""""""""""""""""""""""""""""""""""""""""""""
"function for return the file format and file encoding
"from http://msghost.iyublog.com/?cat=477
function! FunFileEncoding()
return &ff . ":" . &fenc
endfunction
""""""""""""""""""""""""""""""""""""""""""""""""""

set statusline=%2*%=\ \ \ %50(%f\ @%n[%{FunFileEncoding()}]%r%m%y#%{changenr()}%=\ \ \ %-14.(%l,%c\(%v\)%)\ %p%%%)%*
set report=0
set listchars=eol:<
set listchars+=tab:>-
set wildmenu			"用tab在命令行提示
"set clipboard+=unnamed	"是y 和 p 同系统剪切板相连
set clipboard+=autoselect
"set splitbelow 		"splitting a window below the current one
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Files/Backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set autowrite

" in mac os just
" mkdir /var/backups; chmod 755 /var/backups
" ln -s /Volumes/hard/backups/vi/ vi; chmod 777 vi
if has("unix")
    "|| has("win32unix")
    set backupdir=/var/backups/vi
else
    if($MYGENV == "true")
        set backupdir=$E_DISK//var/backups/vi
    else
        set backupdir=e:/var/backups/vi
    endif
endif
"set sessionoptions+=curdir
set browsedir=buffer
set backupskip+=*.clean,cvscommit.????,*.~
set wildignore=*.o,*.obj,*.exe,*.bak,*.clean
if has("gui_running")
	let savevers_dirs = &backupdir
	let savevers_max = 9
	source $ADDED/savevers.vim
	set backup
	set patchmode=.clean
	set backupcopy=yes
endif
"change the direct when chang the buffer
"au   BufEnter *   :lcd %:p:h
set autochdir
set makeef=error.err
set title
set titlelen=80
set titlestring=%<%F\ %M\ %R\ %{v:servername}\ %{mode()}

"""""""""""""""""""""""""""""""""""""""""""""""""""""

"Vim GUI
""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""
" Text Formatting/Layout
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"set fo=tcrqn
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Folding
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set foldenable " Turn on folding
"set foldmethod=indent " Make folding indent sensitive
set foldlevel=100 " Don't autofold anything (but I can still fold manually)
"set foldopen-=search " don't open folds when you search into them
set foldopen-=undo 
"set foldcolumn=8
"set foldmethod=syntax

" setting for xml plugin
let g:xml_syntax_folding = 1
let xml_use_xhtml = 1
"let xml_jump_string = '<!-- mark -->'




"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"nomre mode maping
"nnoremap <C-i> i <esc>r
"nnoremap j gj
"nnoremap k gk
nnoremap ' `

"for caculator ++
nnoremap <C-c> <C-A>

"for del the auto comment
noremap <m-cr> o<esc>^"8d$a

"for eclipse compatible 
inoremap <m-/> <c-x><c-o>

"for good insert next line
"noremap <C-i> ddO
noremap <m-i> :let g:currentline=line('.')<cr>:.d<cr>:if g:currentline==line('.')<cr>:normal -<cr>:endif<cr>o

"use <c-v> to mapping the block visual mode do not using the win32 paste
nnoremap <c-v> <c-v>

"nnoremap <silent> <cr> i<cr><esc>


"insert mode maping
"notice, <c-e> using for end complete mode, <c-y> using for accept complete in
"vim original

" my undo setting
"inoremap <A-r> <C-R>
"inoremap <C-u> <esc>g-a
"inoremap <C-r> <esc>g+a
inoremap <C-u> <C-O>u
"inoremap <C-z> <C-O>u
inoremap <C-r> <C-O><C-R>
"noremap u g-
"noremap <C-r> g+

" Gundo setting
let g:gundo_width = 60
let g:gundo_preview_height = 12
let g:gundo_right = 1
"let g:gundo_close_on_revert = 1
let g:gundo_help = 0
noremap <silent> <m-;> :silent GundoToggle<cr>
inoremap <silent> <m-;> <esc>:silent GundoToggle<cr>
noremap <silent> <m-:> <esc>:silent GundoRenderGraph<cr>
inoremap <silent> <m-:> <esc>:silent GundoRenderGraph<cr>
""""""""""""""""""""""""""""""""

" my window key map setting
noremap <a-j> <c-w>w
inoremap <a-j> <c-w>w
"noremap <a-h> <c-w>h
"inoremap <a-h> <c-w>h
"noremap <a-l> <c-w>l
"inoremap <a-l> <c-w>l
"noremap <a-j> <c-w>j
"inoremap <a-j> <c-w>j
"noremap <a-k> <c-w>k
"inoremap <a-k> <c-w>k
""""""""""""""""""""""""""""""""

" my complete setting
" failure try, must find a way to map /
"inoremap / <kDivide>
"imap <expr> <F3> "\u611B"
"inoremap <Right> <C-R>=pumvisible() ? "\<lt>C-N>" : "\<lt>Down>"<CR>

"like in emacs to kill a line
inoremap <C-k> <space><esc>d$a

"for del the auto comment
inoremap <m-cr> <cr><esc>^d$a

" reproduce previous line word by word
"inoremap <C-j>  @@@<ESC>hhkyWjl?@@@<CR>P/@@@<CR>3s

"visual mode maping
"   Put string marked by visual mode as default for search commands / and ?
"   Warning: extending the marked text by searching doesn't work any more.
vnoremap / y/<C-R>=escape(substitute(@","\n.*",'',''),'/\?*.$^~][')<cr>
vnoremap ? y?<C-R>=escape(substitute(@","\n.*",'',''),'/\?*.$^~][')<cr>
"   Explanation:
" substitute(@",..) - remove lines after first, if >1 line highlighted
" escape(..)        - put '\' before all characters meaningful in regexp's
" y                 - yank current selection (in register ")
" ctrl-r = .. <cr>  - put expression after / as earch string
" credits go to Stefan Roemer and Peppe (Preben Guldberg).

"   Remove whitespace from lines containing nothing else (grr to all nedit users)

"for the mouse conflict key(c-insert) of HP notebook
"noremap <Insert> <Esc>
"noremap <C-Insert> <Esc>
"inoremap <Insert> <Down>
"inoremap <C-Insert> <Down>

"leader mode mapping

"for new tab page setting
map <leader>ta <esc>:tab sball<cr>

"buffer mapping
map <leader>bp <esc>:bp<return>
map <leader>bn <esc>:bn<return>
map <leader>bl <esc>:ls!<cr>

nnoremap <leader>dd <esc>:call MyTriggerMore()<cr>
map <leader>yy <esc>^"*y$

"editor setting mapping
map <leader>ec <esc>:call MyCoding()<cr>
map <leader>el <esc>:set list!<cr>
map <leader>em <esc>:call MyMenu()<cr>
map <leader>en <esc>:set nu!<cr>:set cursorline!<cr>
map <leader>ew <esc>:call MyWrap()<cr>

"file or browser mapping
"map <leader>fx <esc>:Ex<cr>
"nnoremap <leader>fd :set fileformat=dos<cr>
"nnoremap <leader>fu :set fileformat=unix<cr>
"map <leader>ww <esc>:call MyUrl(expand('<cWORD>'))<cr>
"map <leader>m <esc>:silent MarksBrowser<cr>

"search mapping
map <leader>sh <esc>:set hlsearch!<cr>
map <leader>sn <esc>:%s//&/gn<cr>

"spell mapping
map <leader>ss <esc>:setlocal spell!<cr>
"aspell setting
"ca  xsp write !aspell -a --lang="en_US"<cr>
"map <leader>s <Esc>:w<cr><Esc>:!aspell -c --dont-backup --lang=en_US "%:p"<cr>:e! "%"<cr><cr>

"map <leader>df <esc>:silent !start firefox "http://www.m-w.com/cgi-bin/Dictionary?book=Dictionary&va=<cword>"<cr><cr>
"map <leader>dF <esc>:silent !start firefox "http://www.m-w.com/cgi-bin/Thesaurus?book=Thesaurus&va=<cword>"<cr><cr>
"map <leader>dW <esc>:silent !wnb <cword> <cr><cr>

"for new tab setting and new align setting
" leader mode mapping
map <leader>la <esc>:call SecondLoadAlign()<cr>
map <leader>lt <esc>:call SecondTriggerChangeTab()<cr>

" remove space and enter charactor
map <leader>rs <esc>::%s= *$==<cr>
map <leader>rd <esc>:%s/\r//g<cr>


"MAKE IT EASY TO UPDATE/RELOAD_vimrc
nnoremap <leader>-s <esc>:source ~/_vimrc<cr>
nnoremap <leader>-v <esc>:e ~/_vimrc<cr> 

" trigger color style

"if has("gui_running")  "for gdk+ vim seting
    map <leader>cd <esc>:call MyTriggerColor()<cr>
"endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""
" <F5>-<F8>maping for savers-pluging
" <F5> decrease version viewed in VersDiff window
" <F6> increase version viewed in VersDiff window
" <F7> do VersDiff with cvs version of current file
" <F8> cancel VersDiff window
nmap <silent> <F3> :VersDiff -<cr>
nmap <silent> <F4> :VersDiff +<cr>
"""""""""""""""""""""""""""""""""""""""""""""""""""""

" for autohotkey to using:)
" 0=english input 1=chinese input
"let g:cinput = 0
"let g:down = 0

"user command maping
com! MM call MyMore()
com! MLOG call MyLog()
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Programmings abbrevs
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Informational abbrevs
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"iab xda <c-r>=strftime("%Y-%m-%d-%H:%M:%S")<cr>
iab xda <c-r>=substitute(system("date +%Y-%m-%d-%H:%M"),"\n","","")<cr>
iabbrev teh the
iabbrev otehr other
iabbrev wnat want
iabbrev synchronisation
	\ synchronization
iab hte the
iab fro for
iab adn and
iab taht that
iab knwe knew
iab htat that
iab tyr try
iab ture true
"""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""
"for no swap attation
"set dir=~/tmp
func! CheckSwap()
	swapname
	if v:statusmsg =~ '\.sw[^p]$'
		set statusline=%2*%=\ \ \ %50(%f\ @%n%*%1*[$]%*%2*[%{&ff}]%r%m%y%=\ \ \ %-14.(%l,%c\(%v\)%)\ %p%%%)%*
		silent call MyMore()
	else
        set statusline=%2*%=\ \ \ %50(%f\ @%n[%{FunFileEncoding()}]%r%m%y#%{changenr()}%=\ \ \ %-14.(%l,%c\(%v\)%)\ %p%%%)%*
		"set statusline=%2*%=\ \ \ %50(%f\ @%n[%{&ff}]%r%m%y%=\ \ \ %-14.(%l,%c%V%)\ %p%%%)%*
	endif
endfunc

if &swf
	set shm+=A
	autocmd BufEnter * silent call CheckSwap()
endif 
"""""""""""""""""""""""""""""""""""""""""""""""""""""




"""""""""""""""""""""""""""""""""""""""""""""""""""""
"ViKi Mode
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:vikiUseParentSuffix = 1
"let g:vikiSpecialFiles='jpg\|gif\|bmp\|pdf\|dvi\|ps\|eps\|png\|jpeg\|wmf\|otl'
"let g:vikiOpenFileWith_otl = 'silent !D:/prog/Natara/Bonsai/Bonsai.exe %{FILE}'

" we want to allow deplate to execute ruby code and external helper 
" application
"let g:deplatePrg = "deplate -x -X "
"au FileType viki compiler deplate
"let g:vikiInterOLM= $HOME ."/olm"
"let g:vikiInterOLM_suffix= ".olm"
"let g:vikiInterHW= $HOME ."/viki"
"let g:vikiInterHW_suffix= ".txt"




"""""""""""""""""""""""""""""""""""""""""""""""""""""
" setting for mru
map <leader>rr :MRU<cr>gg
let MRU_Max_Entries = 500
let MRU_Exclude_Files = '^/tmp/.*\|^/var/tmp/.*|^c:\\temp\\.*'
let MRU_File = $VIMFILY . "/_vim_mru_files"
"let MRU_Auto_Close = 0  " to keep the mru window
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""
" setting for subversion
nmap <leader>va <Plug>VCSAdd
nmap <leader>vn <Plug>VCSAnnotate
nmap <leader>vc <Plug>VCSCommit
nmap <leader>vd <Plug>VCSDiff
nmap <leader>ve <Plug>VCSEdit
nmap <leader>vi <Plug>VCSEditors
nmap <leader>vg <Plug>VCSGotoOriginal
nmap <leader>vG <Plug>VCSGotoOriginal!
nmap <leader>vl <Plug>VCSLog
nmap <leader>vr <Plug>VCSReview
nmap <leader>vs <Plug>VCSStatus
nmap <leader>vt <Plug>VCSUnedit
nmap <leader>vu <Plug>VCSUpdate
nmap <leader>vv <Plug>VCSVimDiff
nmap <leader>vwv <Plug>VCSWatchers
nmap <leader>vwa <Plug>VCSWatchAdd
nmap <leader>vwn <Plug>VCSWatchOn
nmap <leader>vwf <Plug>VCSWatchOff
nmap <leader>vwf <Plug>VCSWatchRemove
"""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Autocommands
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"autocmd BufEnter * :syntax sync fromstart " ensure every file does syntax highlighting (full)
"autocmd BufEnter * :lcd %:p:h " switch to current dir
"autocmd GUIEnter * :simalt ~x " maximize the screen

augroup filetypedetect 
	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"for help and readbale file mode 
	augroup! filetypedetect 
	autocmd FileType help call MyMore()
	"""""""""""""""""""""""""""""""""""""""""""""""""""""

	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"for the make error windows quickfix windows
	au BufReadPost quickfix set modifiable
	au BufReadPost quickfix  silent g/^/s//\=line(".")." "/
	autocmd FileType qf call MyMore()
	"""""""""""""""""""""""""""""""""""""""""""""""""""""

	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"template mode
	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"autocmd BufNewFile *.sh 		0r ~/vimtemplate/skeleton.sh
	"autocmd BufNewFile *.vim  		0r $HOME/vimfiles/skel/vim.skel
	autocmd BufNewFile,BufReadPre *.java,*.vim,*.sh,*.html,*.xhtml,*.py  		source $ADDED/skel.vim
	"autocmd BufNewFile *.java  		0r $VIMFILY/skel/java.skel
	"autocmd BufNewFile *.vim  		0r $VIMFILY/skel/vim.skel
	"autocmd BufNewFile *.sh  		0r $VIMFILY/skel/sh.skel
	"autocmd BufNewFile *.html  		0r $VIMFILY/skel/html.skel
	"autocmd BufNewFile *.xhtml  	0r $VIMFILY/skel/xhtml.skel
	"autocmd BufNewFile *.java,*.vim,*.sh,*.html,*.xhtml  		call Replacy()
	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"anti word
	"au BufReadPre *.doc set ro
	"au BufReadPre *.doc set hlsearch!
	"au BufReadPost *.doc silent %!antiword "%"
	"autocmd BufReadPost *.doc silent %!wvware "%"
	"""""""""""""""""""""""""""""""""""""""""""""""""""""
	"au BufNewFile,BufRead /home/Administrator/std-java/new1.5/* let g:bnojikes=1

	autocmd BufWritePre,FileWritePre *.vim,_vimrc  ks|call LastMod()|'s
	autocmd BufWritePre,FileWritePre *.jsp,*.html  ks|call LastMod2()|'s

	autocmd BufNewFile,BufreadPre *.text,*.wiki call TextMode()
	"autocmd BufNewFile,BufreadPre *.rst call RstMode()
	autocmd FileType vim setlocal complete=.,k~/dic/vimdiction,w,b,u,i

	autocmd fileType ahk source $ADDED/bracket.vim

augroup end
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"CTags
"""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <silent> <F8> :Tlist<cr>
let Tlist_Show_One_File = 1            " just show tag of current files
let Tlist_Exit_OnlyWindow = 1          " if taglist window iis the last one then exit vim
let Tlist_File_Fold_Auto_Close=1       " auto fold not editing function list
let Tlist_Sort_Type = "name" " order by
let Tlist_Use_Right_Window = 1 " split to the right side of the screen
let tlist_dosini_settings= 'ini;r:respect'
let tlist_html_settings = 'html;a:anchor;f:javascript function;u:url'
let tlist_vb_settings= 'vb;t:type;s:sub;f:function'
let tlist_ant_settings = 'ant;p:Project;r:Property;t:Target'
let JavaBrowser_Use_SingleClick = 0
let JavaBrowser_Use_Right_Window = 1
let JavaBrowser_Use_Icon = 0
let JavaBrowser_Compact_Format = 1
let JavaBrowser_Expand_Tree_At_Startup = 1



"""""""""""""""""""""""""""""""""""""""""""""""""""""
"JavaImp
"""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:JavaImpPaths = $JAVALIB."/j2sdk1.5_src,".$JAVALIB."junit_src,".$VIMFILY."/JavaImp/jmplst"
let g:JavaImpDataDir = $VIMFILY."/JavaImp"
let g:JavaImpDocPaths= $JAVA_DOCPATH
let g:JavaImpDocViewer = "viewHtml"



"""""""""""""""""""""""""""""""""""""""""""""""""""""
"favex mod
"""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:favex_ff='<leader>fa'
let g:favex_fd='<leader>fA'
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"vcscommand mod
augroup VCSCommand
  au VCSCommand User VCSBufferCreated silent! nmap <unique> <buffer> q 		:bwipeout<cr>
augroup END
"let g:VCSCommandEdit= 'spilt'

"""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Buffer Explorer
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:bufExplorerDetailedHelp=1
let g:netrw_keepdir=0
let g:netrw_cygwin= 1
let g:netrw_longlist=1
let g:netrw_sort_by="time"
let g:netrw_sort_direction="reverse"
let g:netrw_timefmt="   %Y %m %d\t%H:%M"
let g:netrw_winsize=25

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"Buffer Select
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"nmap <silent> <unique> <leader>be <Plug>SelectBuf
"nmap <unique> <leader>bl <Plug>SelBufLaunchCmd
"let selBufAlwaysShowHidden=0
"let selBufIgnoreNonFileBufs=0

"Buffer Line
"nmap <Leader>tv :call TabLineSet_verbose_toggle()<CR>
"nmap <Leader>tr :call TabLineSet_verbose_rotate()<CR>
"nmap <silent> <Leader>w :call WinWalkerMenu()<CR>





"Highlight space at end of line as error
"highlight WhitespaceEOL ctermbg=darkred guibg=darkred
"match WhitespaceEOL /\s\+$/




"not use now
"set diffexpr=MyDiff()
"function MyDiff()
"  let opt = '-a --binary '
"  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
"  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
"  let arg1 = v:fname_in
"  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
"  let arg2 = v:fname_new
"  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
"  let arg3 = v:fname_out
"  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
"  silent execute '!e:\prog\Vim\vim63\diff ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
"endfunction
"

function! MyTabLabel(n)
	let buflist = tabpagebuflist(a:n)
	let winnr = tabpagewinnr(a:n)
	return bufname(buflist[winnr - 1])
endfunction

function! MyTabLine()
	let s = ''
	for i in range(tabpagenr('$'))
		" select the highlighting
		if i + 1 == tabpagenr()
		  let s .= '%#TabLineSel#'
		else
		  let s .= '%#TabLine#'
		endif

		" set the tab page number (for mouse clicks)
		let s .= '%' . (i + 1) . 'T'

		" the label is made by MyTabLabel()
		let s .= ' %{MyTabLabel(' . (i + 1) . ')} '
	endfor

	" after the last tab fill with TabLineFill and reset tab page nr
	let s .= '%#TabLineFill#%T'

	" right-align the label to close the current tab page
	if tabpagenr('$') > 1
		let s .= '%=%#TabLine#%999Xclose'
	endif

	return s
endfunction

function! GuiTabLabel()
	let label = ''
	"let bufnrlist = tabpagebuflist(v:lnum)

	" Add '+' if one of the buffers in the tab page is modified
	"for bufnr in bufnrlist
		"if getbufvar(bufnr, "&modified")
			"let label = '+'
			"break
		"endif
	"endfor

	" Append the number of windows in the tab page if more than one
	"let wincount = tabpagewinnr(v:lnum, '$')
	"if wincount > 1
		"let label .= wincount
	"endif
	if v:lnum == g:currentPage
		let label .= ">>"
	endif
	return label

	" Append the buffer name
endfunction

function! GuiTabLabel2()
	let label = ''
	let bufnrlist = tabpagebuflist(v:lnum)

	" Add '+' if one of the buffers in the tab page is modified
	"for bufnr in bufnrlist
		"if getbufvar(bufnr, "&modified")
			"let label = '+'
			"break
		"endif
	"endfor

	" Append the number of windows in the tab page if more than one
	if v:lnum == g:currentPage
		let label .= "<<"
	endif
	let wincount = tabpagewinnr(v:lnum, '$')
	if wincount > 1
		let label .= wincount
	endif
	return label
endfunction

let g:currentPage=1
function! GuiTabLabelEnter()
	let g:currentPage=tabpagenr()
	set guitablabel=%{GuiTabLabel()}%N\ %t\ %{GuiTabLabel2()}
endfunction

function! GuiTabLabelLeave()
endfunction

"	the tab setting and mapping

"set guitablabel=%N\ %f
"set guitablabel=%{GuiTabLabel()}

"set guitablabel=%N/\ %t\ %M
set tabline=%!MyTabLine()
let g:toggleTabs = 0
map <silent><F12> :if g:toggleTabs == 1<CR>:tabo<CR>:set lines+=3<CR>:let g:toggleTabs = 0<CR>:else<CR>:set lines-=3<CR>:tab ball<CR>:let g:toggleTabs = 1<CR>:endif<CR>
map <silent><C-tab> :if g:toggleTabs == 1<CR>:tabnext<CR>:else<CR>:bn<CR>:endif<CR>
map <silent><C-S-tab> :if g:toggleTabs == 1<CR>:tabprevious<CR>:else<CR>:bp<CR>:endif<CR> 

map <leader>tt <esc>:tabnew<cr>:let g:toggleTabs=1<cr>
map <leader>tx :tabclose<cr>:if tabpagenr("$") == 1<cr>let g:toggleTabs=0<cr>:endif<cr><cr>
map <leader>tm :tabmove 
map <leader>tn :tabnext <cr>
map <leader>tp :tabprevious <cr>
"map <C-Tab> :tabn <CR>
"map <C-S-Tab> :tabp <CR> 

"autocmd TabLeave 
autocmd TabEnter * call GuiTabLabelEnter()



if has("gui_running")
    function! InfoGuiTooltip()
        "get window count
        let wincount = tabpagewinnr(tabpagenr(),'$')
        let bufferlist=''
        "get name of active buffers in windows
        for i in tabpagebuflist()
            let bufferlist .= '['.fnamemodify(bufname(i),':t')."]"
        endfor
        return bufname($).'-'.bufferlist
    endfunction
    set guitabtooltip=%!InfoGuiTooltip()


    function! FoldSpellBalloon()
        let foldStart = foldclosed(v:beval_lnum )
        let foldEnd = foldclosedend(v:beval_lnum)
        let lines = []
        " Detect if we are in a fold
        if foldStart < 0
            " Detect if we are on a misspelled word
            let lines = spellsuggest( spellbadword(v:beval_text)[ 0 ], 5, 0 )
        else
            " we are in a fold
            let numLines = foldEnd - foldStart + 1
            " if we have too many lines in fold, show only the first 14
            " and the last 14 lines
            if ( numLines > 31 )
                let lines = getline( foldStart, foldStart + 14 )
                let lines += [ '-- Snipped ' . ( numLines - 30 ) . ' lines --' ]
                let lines += getline( foldEnd - 14, foldEnd )
            else
                "less than 30 lines, lets show all of them
                let lines = getline( foldStart, foldEnd )
            endif
        endif
        " return result
        return join( lines, has( "balloon_multiline" ) ? "\n" : " " )
    endfunction
    set balloonexpr=FoldSpellBalloon()
    set ballooneval
    

endif
    
"for vim im
let g:vimim_chinese_punctuation=0

"for js lint
let jslint_highlight_color = 'yellow'


" for cscope
if has("unix")
    set csprg=/usr/local/bin/cscope
    "set csprg=/usr/bin/mlcscope
else
    set csprg=%CYGWIN_HOME%\bin\\mlcscope.exe
endif

" for clipbord conflict with NERD
nmap <unique> <silent> <Leader>co <Plug>ClipBrdOpen

" add my highlight setting
call MyColorSetting()

" for pathogen vim runtime path manager
call pathogen#infect()

" for snipmate

" for zencoding
"let g:user_zen_leader_key = "<c-\=>"

" for the missing pop up complete frame conflic with enter key
inoremap <expr><Enter> pumvisible() ? "\<C-Y>" : "\<Enter>"

" for dwm.vim
nmap <C-o> <Plug>DWMFocus
